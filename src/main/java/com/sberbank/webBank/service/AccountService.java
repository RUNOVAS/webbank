package com.sberbank.webBank.service;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.Operation;
import com.sberbank.webBank.domain.User;
import com.sberbank.webBank.exception.AccountNotFoundException;
import com.sberbank.webBank.exception.NotEnoughMoneyException;
import com.sberbank.webBank.repos.AccountRepo;
import com.sberbank.webBank.repos.OperationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Date;
import java.util.Calendar;

@Service
public class AccountService {
    @Autowired
    AccountRepo accountRepo;
    @Autowired
    OperationRepo operationRepo;

    public Long createNumber(Long id) {
        Long mask = 4081781000L;
        return mask + id;
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public Long openNewAccount (User owner) {
        Double balance = 0.00;
        Long id = accountRepo.getMaxId();
        if (id == null)
            id = 0L;
        id++;
        Account account = new Account(createNumber(id), balance, owner);
        accountRepo.save(account);
        return account.getNumber();
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean refill(Long number, Double summ) {
        Account account = accountRepo.findByNumber(number);
        account.setBalance(account.getBalance() + summ);
        accountRepo.save(account);
        addRefill(account, summ);
        return true;
    }

    public void addRefill(Account transfer, Double summ) {
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
        Operation operation = new Operation(currentDate, transfer, summ);
        operationRepo.save(operation);
    }

    public void addTransfer(Account account, Account transfer, Double summ) {
        Date currentDate = new Date(Calendar.getInstance().getTime().getTime());
        Operation operation = new Operation(currentDate, account, transfer, summ);
        operationRepo.save(operation);
    }

    @Transactional(isolation = Isolation.SERIALIZABLE)
    public boolean transfer(Long number, Long transferAccount, Double summ) throws AccountNotFoundException, NotEnoughMoneyException {
        Account account = accountRepo.findByNumber(number);
        Account transfer = accountRepo.findByNumber(transferAccount);
        if (account.getBalance() < summ)
            throw new NotEnoughMoneyException();
        if (transfer == null)
            throw new AccountNotFoundException();

        account.setBalance(account.getBalance() - summ);
        transfer.setBalance(transfer.getBalance() + summ);
        accountRepo.save(account);
        accountRepo.save(transfer);
        addTransfer(account, transfer, summ);
        return true;
    }
}
