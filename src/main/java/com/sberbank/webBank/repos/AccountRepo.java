package com.sberbank.webBank.repos;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.math.BigInteger;
import java.util.Set;

public interface AccountRepo extends CrudRepository<Account, Long> {
    Account findByNumber(Long number);

    Set<Account> findByOwner(User owner);

    Long countByNumberAndOwner(Long number, User owner);

    @Query(value = "Select max(id) from account", nativeQuery = true)
    Long getMaxId();
}
