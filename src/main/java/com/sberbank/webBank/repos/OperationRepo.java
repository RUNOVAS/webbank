package com.sberbank.webBank.repos;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.Operation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface OperationRepo extends PagingAndSortingRepository<Operation, Long> {

    Page<Operation> findByAccountOrTransfer(Account account, Account transfer, Pageable pageable);
}
