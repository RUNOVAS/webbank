package com.sberbank.webBank.controller;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.Operation;
import com.sberbank.webBank.domain.User;
import com.sberbank.webBank.repos.AccountRepo;
import com.sberbank.webBank.repos.OperationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/main")
public class OperationController {
    @Autowired
    AccountRepo accountRepo;
    @Autowired
    OperationRepo operationRepo;
    @GetMapping("{account}")
    public String operationList(@AuthenticationPrincipal User user,
                                @PathVariable Account account,
                                Model model,
                                @PageableDefault(sort = { "id" }, direction = Sort.Direction.DESC) Pageable pageable
                                ) {
        Page<Operation> page;
        if(accountRepo.countByNumberAndOwner(account.getNumber(), user) != 1)
            return "/main";
        page = operationRepo.findByAccountOrTransfer(account, account, pageable);
        String url = "/main/" + account.getId();
        model.addAttribute("page", page);
        model.addAttribute("url", url);
        return "history";
    }
}
