package com.sberbank.webBank.controller;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.User;
import com.sberbank.webBank.exception.TransferException;
import com.sberbank.webBank.repos.AccountRepo;
import com.sberbank.webBank.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AccountController {
    @Autowired
    AccountService accountService;
    @Autowired
    AccountRepo accountRepo;

    @GetMapping("/open")
    public String openNewAccount(@AuthenticationPrincipal User owner, Model model) {
        Long account = accountService.openNewAccount(owner);
        model.addAttribute("account", account);
        return "/open";
    }

    @GetMapping("/deposit")
    public String createDeposit(@AuthenticationPrincipal User owner, Model model) {
        Iterable<Account> accounts = accountRepo.findByOwner(owner);
        model.addAttribute("accounts", accounts);
        return "deposit";
    }

    @PostMapping("/deposit")
    public String handleDeposit(@AuthenticationPrincipal User owner,
                                @RequestParam String accountNumber,
                                @RequestParam Double summ,
                                Model model
    ) {
        Long number = Long.parseLong(accountNumber);
        if(accountRepo.countByNumberAndOwner(number, owner) != 1)
            return "/main";
        accountService.refill(number, summ);
        model.addAttribute("account", number);
        model.addAttribute("summ", summ);
        return "/result";
    }

    @GetMapping("/transfer")
    public String createTransfer(@AuthenticationPrincipal User owner, Model model) {
        Iterable<Account> accounts = accountRepo.findByOwner(owner);
        model.addAttribute("accounts", accounts);
        return "transfer";
    }

    @PostMapping("/transfer")
    public String handleTransfer(@AuthenticationPrincipal User owner,
                                @RequestParam String accountNumber,
                                @RequestParam String transferNumber,
                                @RequestParam Double summ,
                                Model model
    ) {
        if (transferNumber == "" || summ == null) {
            model.addAttribute("message", "Transfer Number or Summ is Empty");
            return "result";
        }
        Long number = Long.parseLong(accountNumber);
        Long transfer = Long.parseLong(transferNumber);
        if (accountRepo.countByNumberAndOwner(number, owner) != 1)
            return "/";


        try {
            accountService.transfer(number, transfer, summ);
            model.addAttribute("account", number);
            model.addAttribute("transfer", transfer);
            model.addAttribute("summ", summ);
        } catch (TransferException e) {
            model.addAttribute("message", e.getMessage());
        } finally {
            return "/result";
        }
    }
}
