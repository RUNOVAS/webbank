package com.sberbank.webBank.controller;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.User;
import com.sberbank.webBank.repos.AccountRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class MainController {
    @Autowired
    private AccountRepo accountRepo;

    @GetMapping("/")
    public String greeting(Model model) {
        return "greeting";
    }

    @GetMapping("/main")
    public String main(@AuthenticationPrincipal User owner, Model model) {
        Iterable<Account> accounts = accountRepo.findByOwner(owner);
        model.addAttribute("accounts", accounts);
        return "main";
    }

}
