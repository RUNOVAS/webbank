package com.sberbank.webBank.domain;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Date date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    private Account account;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "transfer_id")
    private Account transfer;
    private Double summ;

    public Operation() {
    }

    public Operation(Date date, Account account, Account transfer, Double summ) {
        this.date = date;
        this.account = account;
        this.transfer = transfer;
        this.summ = summ;
    }

    public Operation(Date date, Account transfer, Double summ) {
        this.date = date;
        this.transfer = transfer;
        this.summ = summ;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getSumm() {
        return summ;
    }

    public void setSumm(Double summ) {
        this.summ = summ;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Account getTransfer() {
        return transfer;
    }

    public void setTransfer(Account transfer) {
        this.transfer = transfer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
