package com.sberbank.webBank.exception;

public class NotEnoughMoneyException extends TransferException {
    @Override
    public String getMessage() {
        return "Not enough money!";
    }
}
