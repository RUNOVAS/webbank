package com.sberbank.webBank.exception;

public class AccountNotFoundException extends TransferException {
    @Override
    public String getMessage() {
        return "Account not found!";
    }
}
