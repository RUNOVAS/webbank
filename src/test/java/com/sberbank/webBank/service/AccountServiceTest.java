package com.sberbank.webBank.service;

import com.sberbank.webBank.domain.Account;
import com.sberbank.webBank.domain.User;
import com.sberbank.webBank.repos.AccountRepo;
import com.sberbank.webBank.repos.OperationRepo;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
class AccountServiceTest {
    @Autowired
    AccountService accountService;

    @MockBean
    AccountRepo accountRepo;
    @MockBean
    OperationRepo operationRepo;

    @Test
    void createNumber() {
        AccountService accountService = new AccountService();
        Assert.assertEquals(Long.valueOf(4081781000L), accountService.createNumber(0L));
        Assert.assertEquals(Long.valueOf(4081781010L), accountService.createNumber(10L));
        Assert.assertEquals(Long.valueOf(4081781100L), accountService.createNumber(100L));
    }

    @Test
    void openNewAccount() {
        Account account = new Account();
        User user = new User();

        Long AccountNumber = accountService.openNewAccount(user);

        Assert.assertNotNull(AccountNumber);
    }
}